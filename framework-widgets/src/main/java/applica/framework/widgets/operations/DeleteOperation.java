package applica.framework.widgets.operations;

import applica.framework.Repository;
import applica.framework.security.Security;
import applica.framework.security.authorization.AuthorizationException;
import applica.framework.security.utils.PermissionUtils;
import applica.framework.widgets.CrudConfigurationException;
import applica.framework.widgets.acl.CrudPermission;

public class DeleteOperation {
    private Repository repository;

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public void delete(String entity, String id) throws CrudConfigurationException, AuthorizationException {
        if (repository == null) throw new CrudConfigurationException("Missing repository");

        PermissionUtils.authorize(Security.withMe().getLoggedUser(), "entity", CrudPermission.DELETE, entity, repository.get(id).orElse(null));

        repository.delete(id);
    }

    public void delete(String entity, String[] ids) throws CrudConfigurationException, AuthorizationException {
        if (repository == null) throw new CrudConfigurationException("Missing repository");

        for (String id : ids) {
            delete(entity, id);
        }
    }
}
